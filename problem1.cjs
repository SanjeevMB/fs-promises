/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


const { rejects } = require('assert');
const { resolveCname } = require('dns/promises');
const fs = require('fs');
const { resolve } = require('path');
const path = require('path');

function createDirectoryAndDeleteTheDirectoryFiles() {

    let directoryName = './createdDirectory';
    let fileRefrenceArray = new Array(10);
    fileRefrenceArray.fill(0);

    function creatingDirectory(directoryName) {

        return new Promise((resolve, rejects) => {

            fs.mkdir(path.join(__dirname, directoryName), (error) => {

                if (error) {

                    rejects(error);

                } else {

                    resolve('directory created');

                }

            });

        });

    }

    let createdDirectory = creatingDirectory(directoryName);

    createdDirectory.then((data) => {

        console.log(data);

    }).catch((data) => {

        console.error(data);

    })

    function filesCreation(index) {

        return new Promise((resolve, rejects) => {

            fs.writeFile(

                `${path.join(__dirname, `${directoryName}`,
                    `file${index + 1}.json`)}`,

                JSON.stringify({ 'name': 'Sanjeev', 'roll_no': 100 }),

                (error) => {

                    if (error) {

                        rejects(error);

                    } else {

                        resolve(`file${index + 1}.json created`);

                    }

                });

        });

    }

    let creationMessage = fileRefrenceArray.map((elment, index, array) => {

        let message = `file${index + 1}.json created`;

        let fileCreated = filesCreation(index);

        fileCreated.then((message) => {

            console.log(message);

        }).catch((message) => {

            console.error(message);

        });

        return message;

    });

    console.log(creationMessage);

    function fileDeletion(fileName, index) {

        return new Promise((resolve, rejects) => {

            fs.unlink(fileName, (error) => {

                if (error) {

                    rejects(error);

                } else {

                    resolve(`file${index + 1}.json deleted`);

                }

            });

        });

    }

    let deletionMessage = fileRefrenceArray.map((nums, index) => {

        let message = `file${index + 1}.json deleted`;

        setTimeout(() => {

            let deletion = fileDeletion(`${path.join(__dirname, `${directoryName}`,
                `file${index + 1}.json`)}`, index);

            deletion.then((message) => {

                console.log(message);

            }).catch((message) => {

                console.error(message)

            });

        }, 2000);

        return message;

    });

    console.log(deletionMessage);

}

module.exports = createDirectoryAndDeleteTheDirectoryFiles;