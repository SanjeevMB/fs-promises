/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the lowerCaseContent, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.        
*/

const { rejects } = require('assert');
const { resolveSoa } = require('dns');
const fs = require('fs');
const { resolve } = require('path');

function readFile(fileName) {

    return new Promise((resolve, rejects) => {

        fs.readFile(fileName, (error, data) => {

            if (error) {

                rejects(error);

            } else {

                resolve(data);

            }

        });

    });

}

function writeFile(fileName, content) {

    return new Promise((resolve, rejects) => {

        fs.writeFile(fileName, content, (error) => {

            if (error) {

                rejects(error);

            } else {

                resolve(fileName);

            }

        });

    });

}

function appendFile(fileName, content) {

    return new Promise((resolve, rejects) => {

        fs.appendFile(fileName, content + '\n', (error) => {

            if (error) {

                rejects(error);

            } else {

                resolve(content);
            }

        });

    });

}

function deleteFile(fileName) {

    return new Promise((resolve, rejects) => {

        fs.unlink(fileName, (error) => {

            if (error) {

                rejects(error);

            } else {

                resolve(`${fileName} deleted`);

            }

        });

    });

}

function mainFunction() {

    readFile('./lipsum.txt').then((data) => {

            // lipsumData
            console.log(data.toString());

            return writeFile('upperCase.txt', data.toString().toUpperCase());

        }).then((data) => {

            appendFile('filename.txt', data);

            return readFile(data);

        }).then((data) => {

            // upperCaseContent
            console.log(data.toString());

            return writeFile('lowerCase.txt', data.toString()
                .toLowerCase()
                .split('.')
                .join('\n'));

        }).then((data) => {

            appendFile('filename.txt', data);

            return readFile(data);

        }).then((data) => {

            // lowerCaseContent
            console.log(data.toString().split('\n')
                .sort()
                .join('\n'));

            // sortedContent
            console.log(data.toString().split('\n')
                .sort()
                .join('\n'));

            return writeFile('sortedContent.txt', data.toString().split('\n')
                .sort()
                .join('\n'));

        }).then((data) => {

            return appendFile('filename.txt', data);

        }).then((data) => {

            return readFile(data);

        }).then((data) => {

            let allFiles = data.toString()
                .split('\n')
                .slice(0, -1);

            let deletedMessage = allFiles.map((file) => {

                let message = `${file} deleted`;

                deleteFile(file).then((data) => {

                    console.log(data);

                }).catch((data) => {

                    console.error(data);

                });

                return message;

            });

            // deleteMessage
            console.log(deletedMessage);

        }).catch((data) => {

            console.log(data);

        });

}

module.exports = mainFunction;